package easyforex.util;

/**
 * Exception for easyforex methods
 * @author damian
 */
public class EasyForexException extends RuntimeException {

	public EasyForexException(String message) {
		super(message);
	}

}
