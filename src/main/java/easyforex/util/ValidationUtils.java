package easyforex.util;

public final class ValidationUtils {

	public static void require(boolean condition, String faultMessage) {
		if (!condition) {
			throw new EasyForexException(faultMessage);
		}
	}
}
